/* 
MIT License

Copyright (c) 2020 Francisco Tirado-Andrés "frta@b105.upm.es"

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

module.exports = {
  init: _init,
  save: _saveDoc,
  find: _findDoc,
  filter: _filterDoc,
  delete: _deleteRow,
};

const pg = require('pg');

var _pool = null;
var _client = null;

var config = {
  user: 'root',
  host: 'localhost',
  database: 'tsst',
  table_name: 'strategies',
  port: 6001,
  timezone: 'UTC+2',
};

function _init(cb) {
  if (_pool) return cb('Already initialized');

  // Connect to database.
  var _pool = new pg.Pool(config);

  _pool.query(
    'CREATE DATABASE IF NOT EXISTS ' + config.database + ';',
    (err, result) => {
      if (err) {
        return console.error('Error executing query:', err.message);
      }
      console.log('Created database with name: ' + config.database);
    }
  );

  _pool.query('USE ' + config.database + ';');
  console.log('Using database with name: ' + config.database);

  _pool.query(
    'CREATE TABLE IF NOT EXISTS ' +
      config.table_name +
      '(' +
      'id UUID PRIMARY KEY DEFAULT gen_random_uuid(),' +
      'email STRING NOT NULL,' +
      'tagname STRING NOT NULL,' +
      'timestamp TIMESTAMPTZ NOT NULL,' +
      'title STRING NOT NULL,' +
      'authors STRING NULL,' +
      'doi STRING NULL,' +
      'url STRING NULL,' +
      'dtapublication TIMESTAMP NULL,' +
      'publisher STRING NULL,' +
      'abstract STRING NULL,' +
      'pseudocode STRING NULL,' +
      'targetDF BOOLEAN NOT NULL,' +
      'targetCT BOOLEAN NOT NULL,' +
      'targetMS BOOLEAN NOT NULL,' +
      'power DECIMAL NOT NULL,' +
      'cost DECIMAL NOT NULL,' +
      'security DECIMAL NOT NULL,' +
      'topology DECIMAL NOT NULL,' +
      'distribution DECIMAL NOT NULL,' +
      'accuracy DECIMAL NOT NULL,' +
      'stability DECIMAL NOT NULL,' +
      'software DECIMAL NOT NULL,' +
      'hardware DECIMAL NOT NULL' +
      ');',
    (err, result) => {
      if (err) {
        return console.error('Error executing query:', err.message);
      }
      console.log(
        'Created table with name: ' + config.database + '.' + config.table_name
      );
    }
  );

  _pool.connect((err, client, done) => {
    if (err) {
      _client = null;
      console.error('Could not connect to db:', err);
      done();
      return cb('Unable to connect');
    }
    _client = client;
    cb();
  });
}

function _saveDoc(data, cb) {
  if (!_client) return cb('Not connected');

  var now = new Date();

  var values = '';
  values += "'" + data.email + "',";
  values += "'" + data.tagname + "',";
  if (data.timestamp == '') {
    values += "'" + now.toJSON() + "',";
  } else {
    values += "'" + data.timestamp + "',";
  }
  values += "'" + data.title + "',";
  values += "'" + data.doi + "',";
  values += "'" + data.url + "',";
  values += "'" + data.publication + "',";
  values += "'" + data.authors + "',";
  values += "'" + data.publisher + "',";
  values += "'" + data.abstract + "',";
  values += "'" + data.pseudocode + "',";
  values += "'" + data.targetDF + "',";
  values += "'" + data.targetCT + "',";
  values += "'" + data.targetMS + "',";
  values += "'" + data.power + "',";
  values += "'" + data.cost + "',";
  values += "'" + data.security + "',";
  values += "'" + data.topology + "',";
  values += "'" + data.distribution + "',";
  values += "'" + data.accuracy + "',";
  values += "'" + data.stability + "',";
  values += "'" + data.software + "',";
  values += "'" + data.hardware + "'";
  var query =
    'UPSERT INTO ' +
    config.table_name +
    ' (email, tagname, timestamp, title, doi, url, dtapublication, \
        authors, publisher, abstract, pseudocode, targetDF, targetCT, targetMS, power, cost, security, topology, \
        distribution, accuracy, stability, software, hardware) VALUES (' +
    values +
    ');';
  console.log('QUERY: ', query);
  _client.query(query, (err) => {
    if (err) console.error('UPSERT failed:', err.message);
    else console.log('UPSERT SAVE OK');
    cb(err ? err.message : null);
  });
}

function _findDoc(data, cb) {
  if (!_client) return cb('Not connected');
  console.log('_findDoc', data);
  var query = 'SELECT * FROM ' + config.table_name + ' WHERE ';
  if (data.targetDF) {
    query += " targetDF = '" + data.targetDF + "'";
  }
  if (data.targetCT) {
    if (data.targetDF) {
      query += ' OR ';
    }
    query += " targetCT = '" + data.targetCT + "'";
  }
  if (data.targetMS) {
    if (data.targetDF | data.targetCT) {
      query += ' OR ';
    }
    query += " targetMS = '" + data.targetMS + "'";
  }
  query += ';';
  console.log('QUERY: ', query);
  _client.query(query, (err, res) => {
    if (err) console.error('SELECT failed:', err.message);
    else console.log('SELECT FIND OK');
    cb(err ? err.message : null, res.rows);
  });
}

function _filterDoc(data, cb) {
  if (!_client) return cb('Not connected');
  var query =
    'SELECT * FROM ' +
    config.table_name +
    ' where (' +
    data.targetname +
    ' = true ' +
    ') AND (power <= ' +
    data.power +
    ') AND (cost <= ' +
    data.cost +
    ') AND (security <= ' +
    data.security +
    ') AND (topology <= ' +
    data.topology +
    ') AND (distribution <= ' +
    data.distribution +
    ') AND (accuracy <= ' +
    data.accuracy +
    ') AND (stability <= ' +
    data.stability +
    ') AND (software <= ' +
    data.software +
    ') AND (hardware <= ' +
    data.hardware +
    ');';
  console.log('QUERY: ', query);
  _client.query(query, (err, res) => {
    if (err) console.error('SELECT failed:', err.message);
    else console.log('SELECT FILTER OK');
    cb(err ? err.message : null, res.rows);
  });
}

function _deleteRow(data, cb) {
  if (!_client) return cb('Not connected');
  var query =
    'DELETE FROM ' + config.table_name + " WHERE id = '" + data.id + "';";
  console.log('QUERY: ', query);
  _client.query(query, (err, res) => {
    if (err) console.error('SELECT failed:', err.message);
    else console.log('SELECT DELETE OK');
    cb(err ? err.message : null, res.rows);
  });
}
