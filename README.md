# README #

The Time-Synchronization Search Tool (TSST) is a tool that has been created to facilitate the use of 
the methodology developed in the doctoral thesis called 
"Methodology for implementation of Synchronization Strategies for Wireless Sensor Networks". 
This tool is divided into three sections: administration, user and associated tools.

## Demo for TSST ##

* [Time-Synchronization Search Tool Demo](http://bender.die.upm.es:3000/tsst/index.html )