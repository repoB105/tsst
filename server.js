/* 
MIT License

Copyright (c) 2020 Francisco Tirado-Andrés "frta@b105.upm.es"

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

"use strict";

// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'nodeServer: TSST';


// http servers
var http = require('http');
var url = require("url");
var fs = require('fs');
var path = require('path');
//
var modules = {
  // Init CockroachDB
  db: require('./db')
}

/**
 * Global variables
 */
var webroot = path.join(__dirname, 'www');
console.log('webroot --> ' + webroot);
var favicon = fs.readFileSync(path.join(webroot, '/images/favicon.ico'));

// Init modules
for (var k in modules) {
  modules[k].init((err) => {
    if (err) {
      console.error('Unable to initialize module', k, ':', err);
      process.exit(1);
    }
  });
}

/**
 * HTTP server
 */
var server = http.createServer(httpHandler);

function httpHandler(request, response) {
  var myurl = url.parse(request.url);
  console.log((new Date()) + ' HTTP server. URL'
    + request.url + ' requested.');
  console.log('Request: ' + myurl.path);
  try {
    switch (request.method.toUpperCase()) {
      case 'GET':
        if (myurl.pathname == '/images/favicon.ico') {
          console.log('favicon found!');
          response.writeHead(200, { 'Content-Type': 'image/x-icon' });
          response.end(favicon, 'binary');
          return;
        }
        if (myurl.pathname == '/') {
          myurl.pathname = '/index.html';
        }
        var filePath = path.join(webroot, myurl.pathname);
        console.log('File path: ', filePath);
        fs.readFile(filePath, function (err, data) {
          if (err) {
            response.writeHead(404, { 'Content-Type': 'text/plain' });
            response.end('File not found');
          } else {
            response.writeHead(200, { 'Content-Type': getContentType(myurl.pathname) });
            response.end(data);
          }
        });
        break;
      case 'POST':
        var body = request.body || '';
        request.on('data', (chunk) => {
          body += chunk;
        }).on('end', () => {
          console.log('body:', body);
          var data = JSON.parse(body);
          var cmd = myurl.path.split('/');
          if (!cmd[0]) cmd = cmd.slice(1);
          var mod = modules[cmd[0]];
          var fun = mod[cmd[1]];
          fun(data, (err, res) => {
            response.writeHead(200, { 'Content-Type': 'application/json' });
            response.end(JSON.stringify({
              err: err,
              res: res
            }));
          });
        });
        break;
      default: throw new Error('Unknown method on request: ' + request.method);
    }
  }
  catch (err) {
    console.error('Handler error: ' + err);
    response.writeHead(500);
    response.end('Internal server error');
  }
}

// Init HTTP server 
var httpPort = process.argv[2] || 80;
server.listen(httpPort, function () {
  console.log('[INFO] ' + (new Date()));
  console.log('[INFO] ' + "Server is listening on: " + server.address().address + ":" + server.address().port);
});

function getContentType(filePath) {
  var ext = path.extname(filePath);
  var contentType = 'text/html';
  switch (ext) {
    case '.js':
      contentType = 'text/javascript';
      break;
    case '.css':
      contentType = 'text/css';
      break;
    case '.json':
      contentType = 'application/json';
      break;
    case '.png':
      contentType = 'image/png';
      break;
    case '.jpg':
      contentType = 'image/jpg';
      break;
    case '.wav':
      contentType = 'audio/wav';
      break;
    default:
      break;
  }
  return contentType;
}