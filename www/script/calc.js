/* 
MIT License

Copyright (c) 2020 Francisco Tirado-Andrés "frta@b105.upm.es"

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

var _enableLog = true;

$(document).ready(function () {
  try {
    // Main layout
    $.jqx.theme = 'bootstrap';
    $('#notifier').jqxNotification({
      width: 'auto',
      position: 'bottom-right',
      opacity: 1,
      autoOpen: false,
      animationOpenDelay: 600,
      autoClose: true,
      autoCloseDelay: 5000,
    });
    _init();
  } catch (ex) {
    alert(ex.stack, 'error');
  }
});

function _notify(msg, template) {
  template = typeof template !== 'undefined' ? template : 'info';
  $('#notifier').jqxNotification({ template: template, autoClose: true });
  $('#notifier span').text(msg);
  $('#notifier').jqxNotification('open');
}

function _init(data) {
  _initControls();
  //_validateOptions();
}

function _initControls() {
  var valueqPPM = 20;
  var valueqSec = 1.728;
  var valueqMin = 10.512;
  var deviation = 1.05;
  var valueErrSec = 0.09;
  var valueErrMin = 0.55;

  // calculadora RTC
  $('#qPPM').jqxInput({ value: 20 });
  $('#btnClkAppm').on('click', function (event) {
    valueqPPM = Number($('#qPPM').val());
    valueqSec = Number((60 * 60 * 24 * valueqPPM) / Math.pow(10, 6));
    valueqMin = Number((60 * 24 * 365 * valueqPPM) / Math.pow(10, 6));
    $('#qSec').val(valueqSec.toFixed(3));
    $('#qMin').val(valueqMin.toFixed(3));
    // Accumulated
    $('#AccuPPM').val((valueqPPM + deviation).toFixed(3));
    $('#AccuSec').val((valueqSec + valueErrSec).toFixed(3));
    $('#AccuMin').val((valueqMin + valueErrMin).toFixed(3));
  });

  $('#qSec').jqxInput({ value: 1.728 });
  $('#btnClkAsec').on('click', function (event) {
    valueqSec = Number($('#qSec').val());
    valueqPPM = Number((valueqSec * Math.pow(10, 6)) / (60 * 60 * 24));
    valueqMin = Number((valueqSec * 365) / 60);
    $('#qPPM').val(valueqPPM.toFixed(3));
    $('#qMin').val(valueqMin.toFixed(3));
    // Accumulated
    $('#AccuPPM').val((valueqPPM + deviation).toFixed(3));
    $('#AccuSec').val((valueqSec + valueErrSec).toFixed(3));
    $('#AccuMin').val((valueqMin + valueErrMin).toFixed(3));
  });

  $('#qMin').jqxInput({ value: 10.512 });
  $('#btnClkAmin').on('click', function (event) {
    valueqMin = Number($('#qMin').val());
    valueqPPM = Number((valueqMin * Math.pow(10, 6)) / (60 * 24 * 365));
    valueqSec = Number((valueqMin * 60) / 365);
    $('#qPPM').val(valueqPPM.toFixed(3));
    $('#qSec').val(valueqSec.toFixed(3));
    // Accumulated
    $('#AccuPPM').val((valueqPPM + deviation).toFixed(3));
    $('#AccuSec').val((valueqSec + valueErrSec).toFixed(3));
    $('#AccuMin').val((valueqMin + valueErrMin).toFixed(3));
  });

  // Calculadora Temperatura error
  $('#TempOperating').jqxInput({ value: 20.0 });
  $('#CteTemp').jqxInput({ value: 0.042 });
  $('#XOTempErrPPM').jqxInput({ value: 1.05, disabled: true });
  $('#XOTempErrSec').jqxInput({ value: 0.09, disabled: true });
  $('#XOTempErrMin').jqxInput({ value: 0.55, disabled: true });

  $('#btnClkB').jqxButton({ disabled: false });
  $('#btnClkB').on('click', function () {
    var valueTempOperating = $('#TempOperating').jqxInput('val');
    var valueCteTemp = $('#CteTemp').jqxInput('val');
    deviation = Number(valueCteTemp * Math.pow(25 - valueTempOperating, 2));
    valueErrSec = Number((60 * 60 * 24 * deviation) / Math.pow(10, 6));
    valueErrMin = Number((60 * 24 * 365 * deviation) / Math.pow(10, 6));
    $('#XOTempErrPPM').val(deviation.toFixed(3));
    $('#XOTempErrSec').val(valueErrSec.toFixed(3));
    $('#XOTempErrMin').val(valueErrMin.toFixed(3));
    // Accumulated
    $('#AccuPPM').val((valueqPPM + deviation).toFixed(3));
    $('#AccuSec').val((valueqSec + valueErrSec).toFixed(3));
    $('#AccuMin').val((valueqMin + valueErrMin).toFixed(3));
  });

  $('#AccuPPM').jqxInput({ value: 21.05, disabled: true });
  $('#AccuSec').jqxInput({ value: 1.818, disabled: true });
  $('#AccuMin').jqxInput({ value: 11.062, disabled: true });

  // ppm converter
  $('#decimal').jqxInput({ value: '0.00001' });
  $('#btnDecimal').on('click', function () {
    var value = $('#decimal').jqxInput('val');
    $('#percent').jqxInput('val', value * 100);
    $('#permille').jqxInput('val', value * 1000);
    $('#ppm').jqxInput('val', value * 1000000);
    $('#ppb').jqxInput('val', value * 1000000000);
    $('#ppt').jqxInput('val', value * 1000000000000);
  });
  $('#percent').jqxInput({ value: '0.001' });
  $('#btnPercent').on('click', function () {
    var value = $('#percent').jqxInput('val');
    $('#decimal').jqxInput('val', value / 100);
    $('#permille').jqxInput('val', value * 10);
    $('#ppm').jqxInput('val', value * 10000);
    $('#ppb').jqxInput('val', value * 10000000);
    $('#ppt').jqxInput('val', value * 10000000000);
  });
  $('#permille').jqxInput({ value: '0.01' });
  $('#btnPermille').on('click', function () {
    var value = $('#permille').jqxInput('val');
    $('#decimal').jqxInput('val', value / 1000);
    $('#percent').jqxInput('val', value / 10);
    $('#ppm').jqxInput('val', value * 1000);
    $('#ppb').jqxInput('val', value * 1000000);
    $('#ppt').jqxInput('val', value * 1000000000);
  });
  $('#ppm').jqxInput({ value: '10.0' });
  $('#btnPpm').on('click', function () {
    var value = $('#ppm').jqxInput('val');
    $('#decimal').jqxInput('val', value / 1000000);
    $('#percent').jqxInput('val', value / 10000);
    $('#permille').jqxInput('val', value / 1000);
    $('#ppb').jqxInput('val', value * 1000);
    $('#ppt').jqxInput('val', value * 1000000);
  });
  $('#ppb').jqxInput({ value: '10000' });
  $('#btnPpb').on('click', function () {
    var value = $('#ppb').jqxInput('val');
    $('#decimal').jqxInput('val', value / 1000000000);
    $('#percent').jqxInput('val', value / 10000000);
    $('#permille').jqxInput('val', value / 1000000);
    $('#ppm').jqxInput('val', value / 1000);
    $('#ppt').jqxInput('val', value * 1000);
  });
  $('#ppt').jqxInput({ value: '10000000' });
  $('#btnPpt').on('click', function () {
    var value = $('#ppt').jqxInput('val');
    $('#decimal').jqxInput('val', value / 1000000000000);
    $('#percent').jqxInput('val', value / 10000000000);
    $('#permille').jqxInput('val', value / 1000000000);
    $('#ppm').jqxInput('val', value / 1000000);
    $('#ppb').jqxInput('val', value / 1000);
  });

  //error clock quality tradeoff
  $('#clockQ').jqxInput({ value: '20' });
  $('#btnClockQ').on('click', function () {
    var roundi = $('#roundi').val();
    var errorMax = $('#errorsyn').val();
    $('#clockQ').val((errorMax * 1000000) / roundi);
  });

  $('#errorsyn').jqxInput({ value: '1' });
  $('#btnErrorsyn').on('click', function () {
    var clockQ = $('#clockQ').val();
    var roundi = $('#roundi').val();
    $('#errorsyn').val((roundi * clockQ) / 1000000);
  });

  $('#roundi').jqxInput({ value: '50000' });
  $('#btnRoundi').on('click', function () {
    var clockQ = $('#clockQ').val();
    var errorMax = $('#errorsyn').val();
    $('#roundi').val((errorMax * 1000000) / clockQ);
  });

  // Button: Back
  $('#btnBack').jqxButton({ disabled: false });
  $('#btnBack').on('click', function () {
    if ($('#btnBack').jqxButton('disabled')) return;
    window.location = 'index.html';
  });
}

function _log(text) {
  if (_enableLog)
    console.log(new Date().toISOString().slice(-13, -1) + ' | ' + text);
}
