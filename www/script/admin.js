/* 
MIT License

Copyright (c) 2020 Francisco Tirado-Andrés "frta@b105.upm.es"

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

const MS_PER_DAY = 86400000;

var _enableLog = true;
var _areas = [];
var _bounddata = '';

$(document).ready(function () {
  try {
    // Main layout
    $.jqx.theme = 'bootstrap';
    $('#notifier').jqxNotification({
      width: 'auto',
      position: 'bottom-right',
      opacity: 1,
      autoOpen: false,
      animationOpenDelay: 600,
      autoClose: true,
      autoCloseDelay: 5000,
    });
    _init();
  } catch (ex) {
    alert(ex.stack, 'error');
  }
});

function _notify(msg, template) {
  template = typeof template !== 'undefined' ? template : 'info';
  $('#notifier').jqxNotification({ template: template, autoClose: true });
  $('#notifier span').text(msg);
  $('#notifier').jqxNotification('open');
}

function _init(data) {
  //_checkPwd();
  _initControls();
  _validateOptions();
}

function _checkPwd() {
  /* ToDo. Authenticantion with login and admin account */
  var password = prompt(
    'Please enter the administrator password to view this page!'
  );
  if (password != 'aTSST') {
    alert('Password Failed! Going back to index.');
    window.location = 'index.html';
  } else {
    _notify('Password Correct!', 'success');
  }
}

function _initControls() {
  //Title
  $('#headerTitle').text('Administration TSST');

  // Text input: admin email
  $('#txtEmail').jqxInput({ source: [] });
  $('#txtEmail').on('change', _validateOptions);
  $('#txtEmail').jqxTooltip({
    content: '<b>REQUIRED</b>. Please enter the administrator contact email.',
    position: 'bottom',
    name: 'formTooltip',
  });

  // Text input: Tagname
  $('#txtTag').jqxInput({ source: [] });
  $('#txtTag').on('change', _validateOptions);
  $('#txtTag').jqxTooltip({
    content: '<b>REQUIRED</b>. Please enter a unique short name.',
    position: 'bottom',
    name: 'formTooltip',
  });

  // Text input: Timestamp
  $('#txtTimestamp').jqxInput({ source: [] });

  // DateTime Input: Timestamp
  var now = Date.now();
  var today = now - (now % MS_PER_DAY);

  // Text input: Title
  $('#txtTitle').jqxInput({ source: [] });
  $('#txtTitle').on('change', _validateOptions);
  $('#txtTitle').jqxTooltip({
    content: '<b>REQUIRED</b>. Please enter the Full title of the strategy.',
    position: 'bottom',
    name: 'formTooltip',
  });

  // Text input: Authors
  $('#txtAuthors').jqxInput({ source: [] });
  $('#txtAuthors').on('change', _validateOptions);
  $('#txtAuthors').jqxTooltip({
    content: 'Please enter the name of the authors separated by ;',
    position: 'bottom',
    name: 'formTooltip',
  });

  // // Text input: DOI
  $('#txtDoi').jqxInput({ source: [] });
  $('#txtDoi').on('change', _validateOptions);
  $('#txtDoi').jqxTooltip({
    content: 'Please enter a valid DOI number.',
    position: 'bottom',
    name: 'formTooltip',
  });

  // Text input: URL
  $('#txtUrl').jqxInput({ source: [] });
  $('#txtUrl').on('change', _validateOptions);
  $('#txtUrl').jqxTooltip({
    content: 'Please enter a valid URL address.',
    position: 'bottom',
    name: 'formTooltip',
  });

  //DateTime Input: Publication Date
  $('#dtpPublica').jqxDateTimeInput({
    width: '300px',
    height: '25px',
    formatString: 'D',
  });
  $('#dtpPublica').jqxDateTimeInput('setDate', new Date(today));
  $('#dtpPublica').on('change', _validateOptions);
  $('#dtpPublica').jqxTooltip({
    content: 'Please enter a valid date.',
    position: 'bottom',
    name: 'formTooltip',
  });

  // Text input: Publisher
  $('#txtPublisher').jqxInput({ source: [] });
  $('#txtPublisher').on('change', _validateOptions);
  $('#txtPublisher').jqxTooltip({
    content: 'Please enter the Journal publisher.',
    position: 'bottom',
    name: 'formTooltip',
  });

  // Text input: Abstract
  $('#txtAbstract').jqxInput({ source: [] });
  $('#txtAbstract').on('change', _validateOptions);
  $('#txtAbstract').jqxTooltip({
    content: 'Please enter the abstract of the publication.',
    position: 'bottom',
    name: 'formTooltip',
  });

  // Text input: Pseudocode
  $('#txtPsudocode').jqxInput({ source: [] });
  $('#txtPsudocode').on('change', _validateOptions);
  $('#txtPsudocode').jqxTooltip({
    content: 'Please enter the pseudocode if any.',
    position: 'bottom',
    name: 'formTooltip',
  });

  /* info popover */
  $('[data-toggle="popover"]').popover({
    html: true,
    container: 'body',
    content: function () {
      var content = $(this).attr('data-popover-content');
      return $(content).children('.popover-body').html();
    },
    title: function () {
      var title = $(this).attr('data-popover-content');
      return $(title).children('.popover-heading').html();
    },
  });

  /* Only one popover at a time */
  $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
      //the 'is' for buttons that trigger popups
      //the 'has' for icons within a button that triggers a popup
      if (
        !$(this).is(e.target) &&
        $(this).has(e.target).length === 0 &&
        $('.popover').has(e.target).length === 0
      ) {
        $(this).popover('hide');
      }
    });
  });

  //Checkbox type: Target
  $('#targetDF').jqxCheckBox({});
  $('#targetDF').on('change', _validateOptions);
  $('#targetCT').jqxCheckBox({});
  $('#targetCT').on('change', _validateOptions);
  $('#targetMS').jqxCheckBox({});
  $('#targetMS').on('change', _validateOptions);
  $('#targetCheck').css({
    'border-style': 'solid',
    'border-width': '1px',
    'border-color': '#ff0000',
  });

  // Power
  $('#sldPower').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 0,
    max: 9,
    ticksFrequency: 1,
    value: 0,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 0) return value;
      if (value == 9) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value >= 1 && value < 2) {
        var myValue = (value - 1) * 45 + 5;
        return '<' + myValue + 'µW';
      } else if (value >= 2 && value < 3) {
        var myValue = (value - 2) * 450 + 50;
        return '<' + myValue + 'µW';
      } else if (value >= 3 && value < 4) {
        var myValue = (value - 3) * 500 + 500;
        return '<' + myValue + 'µW';
      } else if (value >= 4 && value < 5) {
        var myValue = (value - 4) * 4 + 1;
        return '<' + myValue + 'mW';
      } else if (value >= 5 && value < 6) {
        var myValue = (value - 5) * 45 + 5;
        return '<' + myValue + 'mW';
      } else if (value >= 6 && value < 7) {
        var myValue = (value - 6) * 450 + 50;
        return '<' + myValue + 'mW';
      } else if (value >= 7 && value < 8) {
        var myValue = (value - 7) * 500 + 500;
        return '<' + myValue + 'mW';
      } else if (value >= 8 && value <= 9) {
        var myValue = (value - 8) * 4 + 1;
        return '<' + myValue + 'W';
      } else if (value == 0) return 'Not rated';
      else return value;
    },
  });
  // Validation
  $('#sldPower').on('change', function (event) {
    var value = event.args.value;
    if (value < 1) {
      $('#sldPower').jqxSlider('val', 0);
    }
  });

  // Money
  $('#sldCost').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 0,
    max: 9,
    ticksFrequency: 1,
    value: 0,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 0) return value;
      if (value == 9) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value >= 1 && value < 2) {
        var myValue = (value - 1) * 14 + 1;
        return '<' + myValue + '%';
      } else if (value >= 2 && value < 3) {
        var myValue = (value - 2) * 15 + 15;
        return '<' + myValue + '%';
      } else if (value >= 3 && value < 4) {
        var myValue = (value - 3) * 10 + 30;
        return '<' + myValue + '%';
      } else if (value >= 4 && value < 5) {
        var myValue = (value - 4) * 10 + 40;
        return '<' + myValue + '%';
      } else if (value >= 5 && value < 6) {
        var myValue = (value - 5) * 10 + 50;
        return '<' + myValue + '%';
      } else if (value >= 6 && value < 7) {
        var myValue = (value - 6) * 15 + 60;
        return '<' + myValue + '%';
      } else if (value >= 7 && value < 8) {
        var myValue = (value - 7) * 15 + 75;
        return '<' + myValue + '%';
      } else if (value >= 8 && value <= 9) {
        var myValue = (value - 8) * 10 + 90;
        return '<' + myValue + '%';
      } else if (value == 0) return 'Not rated';
      else return value;
    },
  });
  // Validation
  $('#sldCost').on('change', function (event) {
    var value = event.args.value;
    if (value < 1) {
      $('#sldCost').jqxSlider('val', 0);
    }
  });

  // // Text input: Security
  // Security
  $('#sldSecurity').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 0,
    max: 9,
    ticksFrequency: 1,
    value: 0,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 0) return value;
      if (value == 9) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 0) return 'Not rated';
      else return value;
    },
  });
  // Validation
  $('#sldSecurity').on('change', function (event) {
    var value = event.args.value;
    if (value < 1) {
      $('#sldSecurity').jqxSlider('val', 0);
    }
  });

  // // Text input: Topology
  // Topology
  $('#sldTopology').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 0,
    max: 9,
    ticksFrequency: 1,
    value: 0,
    step: 1,
    tickLabelFormatFunction: function (value) {
      if (value == 0) return value;
      if (value == 9) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 1) return 'Mesh';
      else if (value == 3) return 'Tree';
      else if (value == 5) return 'Star';
      else if (value == 7) return 'Linear';
      else if (value == 9) return 'All valid';
      else if (value == 0) return 'Not rated';
      else return value;
    },
  });
  // Validation
  $('#sldTopology').on('change', function (event) {
    var value = event.args.value;
    if (value != 0 && !(value % 2)) {
      value += 1;
      $('#sldTopology').jqxSlider('val', value);
    }
  });

  // // Text input: Distribution
  // Distribution
  $('#sldDistribution').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 0,
    max: 9,
    ticksFrequency: 1,
    value: 0,
    step: 1,
    tickLabelFormatFunction: function (value) {
      if (value == 0) return value;
      if (value == 9) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 1) return 'Multi-Hop Broadcast';
      else if (value == 3) return 'Multi-Hop Point-to-Point';
      else if (value == 5) return 'Multi-Hop Flooding';
      else if (value == 7) return 'One-Hop Broadcast';
      else if (value == 9) return 'One-Hop Point-to-Point';
      else if (value == 0) return 'Not rated';
      else return value;
    },
  });
  // Validation
  $('#sldDistribution').on('change', function (event) {
    var value = event.args.value;
    if (value != 0 && !(value % 2)) {
      value += 1;
      $('#sldDistribution').jqxSlider('val', value);
    }
  });

  // // Text input: Accuracy
  // Accuracy
  $('#sldAccuracy').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 0,
    max: 9,
    ticksFrequency: 1,
    value: 0,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 0) return value;
      if (value == 9) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value >= 1 && value < 2) {
        var myValue = (value - 1) * 9 + 1;
        return '<' + myValue + 'µs';
      } else if (value >= 2 && value < 3) {
        var myValue = (value - 2) * 490 + 10;
        return '<' + myValue + 'µs';
      } else if (value >= 3 && value < 4) {
        var myValue = (value - 3) * 500 + 500;
        return '<' + myValue + 'µs';
      } else if (value >= 4 && value < 5) {
        var myValue = (value - 4) * 9 + 1;
        return '<' + myValue + 'ms';
      } else if (value >= 5 && value < 6) {
        var myValue = (value - 5) * 490 + 10;
        return '<' + myValue + 'ms';
      } else if (value >= 6 && value < 7) {
        var myValue = (value - 6) * 500 + 500;
        return '<' + myValue + 'ms';
      } else if (value >= 7 && value < 8) {
        var myValue = (value - 7) * 9 + 1;
        return '<' + myValue + 's';
      } else if (value >= 8 && value <= 9) {
        var myValue = (value - 8) * 50 + 10;
        return '<' + myValue + 's';
      } else if (value == 0) return 'Not rated';
      else return value;
    },
  });
  // Validation
  $('#sldAccuracy').on('change', function (event) {
    var value = event.args.value;
    if (value < 1) {
      $('#sldAccuracy').jqxSlider('val', 0);
    }
  });

  // // Text input: Stability
  // Stability
  $('#sldStability').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 0,
    max: 9,
    ticksFrequency: 1,
    value: 0,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 0) return value;
      if (value == 9) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value >= 1 && value < 2) {
        var myValue = (value - 1) * 0.09 + 0.01;
        return '<' + myValue + 'ppb';
      } else if (value >= 2 && value < 3) {
        var myValue = (value - 2) * 0.9 + 0.1;
        return '<' + myValue + 'ppb';
      } else if (value >= 3 && value < 4) {
        var myValue = (value - 3) * 9 + 1;
        return '<' + myValue + 'ppb';
      } else if (value >= 4 && value < 5) {
        var myValue = (value - 4) * 0.09 + 0.01;
        return '<' + myValue + 'ppm';
      } else if (value >= 5 && value < 6) {
        var myValue = (value - 5) * 0.9 + 0.1;
        return '<' + myValue + 'ppm';
      } else if (value >= 6 && value < 7) {
        var myValue = (value - 6) * 9 + 1;
        return '<' + myValue + 'ppm';
      } else if (value >= 7 && value < 8) {
        var myValue = (value - 7) * 90 + 10;
        return '<' + myValue + 'ppm';
      } else if (value >= 8 && value <= 9) {
        var myValue = (value - 8) * 900 + 100;
        return '<' + myValue + 'ppm';
      } else if (value == 0) return 'Not rated';
      else return value;
    },
  });
  // Validation
  $('#sldStability').on('change', function (event) {
    var value = event.args.value;
    if (value < 1) {
      $('#sldStability').jqxSlider('val', 0);
    }
  });

  // // Text input: Software
  // Software
  $('#sldSoftware').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 0,
    max: 8,
    ticksFrequency: 1,
    value: 0,
    step: 1,
    tickLabelFormatFunction: function (value) {
      if (value == 0) return value;
      if (value == 8) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 1) return 'APP OS';
      else if (value == 2) return 'APP BM';
      else if (value == 3) return 'OS';
      else if (value == 4) return 'NET';
      else if (value == 5) return 'MAC';
      else if (value == 6) return 'HAL';
      else if (value == 7) return 'Driver';
      else if (value == 8) return 'Register';
      else if (value == 0) return 'Not rated';
      else return value;
    },
  });
  // Validation
  $('#sldSoftware').on('change', function (event) {
    var value = event.args.value;
    if (value < 1) {
      $('#sldSoftware').jqxSlider('val', 0);
    }
  });

  // // Text input: Hardware
  // Hardware
  $('#sldHardware').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 0,
    max: 7,
    ticksFrequency: 1,
    value: 0,
    step: 1,
    tickLabelFormatFunction: function (value) {
      if (value == 0) return value;
      if (value == 7) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 1) return 'HW-NS Low Res';
      else if (value == 2) return 'HW-NS High Res';
      else if (value == 3) return 'Additonal OSC';
      else if (value == 4) return 'External Module';
      else if (value == 5) return 'TX/RX specific';
      else if (value == 6) return 'MCU specific';
      else if (value == 7) return 'Platform specific';
      else if (value == 0) return 'Not rated';
      else return value;
    },
  });
  // Validation
  $('#sldHardware').on('change', function (event) {
    var value = event.args.value;
    if (value < 1) {
      $('#sldHardware').jqxSlider('val', 0);
    }
  });

  // Button: Guardar
  $('#btnSave').jqxButton({ disabled: true });
  $('#btnSave').jqxButton({ value: 'Save' });
  $('#btnSave').on('click', function () {
    if ($('#btnSave').jqxButton('disabled')) return;
    //_sendSave();
    _notify("ACCESS DENIED: You don't have permission to save", 'error');
  });

  // Button: Buscar
  $('#btnFind').jqxButton({ disabled: true });
  $('#btnFind').on('click', function () {
    if ($('#btnFind').jqxButton('disabled')) return;
    _sendFind();
  });

  // Button: Limpiar
  $('#btnClean').jqxButton({ disabled: true });
  $('#btnClean').on('click', function () {
    if ($('#btnClean').jqxButton('disabled')) return;
    _sendClean();
  });

  // Button: Borrar
  $('#btnDelete').jqxButton({ disabled: true });
  $('#btnDelete').on('click', function () {
    if ($('#btnDelete').jqxButton('disabled')) return;
    //_sendDelete();
    _notify("ACCESS DENIED: You don't have permission to delete", 'error');
  });

  // Button: Back
  $('#btnBack').jqxButton({ disabled: false });
  $('#btnBack').on('click', function () {
    if ($('#btnBack').jqxButton('disabled')) return;
    window.location = 'index.html';
  });
}

function _validateOptions() {
  var ok = true;
  var num;

  //Dropdown type: target
  // bind to change event.
  $('#targetDF').bind('change', function (event) {
    var checkedDF = event.args.checked;
    var checkedCT = $('#targetCT').jqxCheckBox('checked');
    var checkedMS = $('#targetMS').jqxCheckBox('checked');
    if (checkedDF | checkedCT | checkedMS) {
      $('#targetCheck').css({
        'border-style': 'none',
        'border-width': '0px',
        'border-color': '',
      });
      ok = false;
    } else {
      $('#targetCheck').css({
        'border-style': 'solid',
        'border-width': '1px',
        'border-color': '#ff0000',
      });
      ok = true;
    }
    $('#btnFind').jqxButton({ disabled: ok });
    $('#btnClean').jqxButton({ disabled: ok });
  });
  $('#targetCT').bind('change', function (event) {
    var checkedDF = $('#targetDF').jqxCheckBox('checked');
    var checkedCT = event.args.checked;
    var checkedMS = $('#targetMS').jqxCheckBox('checked');
    if (checkedDF | checkedCT | checkedMS) {
      $('#targetCheck').css({
        'border-style': 'none',
        'border-width': '0px',
        'border-color': '',
      });
      ok = false;
    } else {
      $('#targetCheck').css({
        'border-style': 'solid',
        'border-width': '1px',
        'border-color': '#ff0000',
      });
      ok = true;
    }
    $('#btnFind').jqxButton({ disabled: ok });
    $('#btnClean').jqxButton({ disabled: ok });
  });
  $('#targetMS').bind('change', function (event) {
    var checkedDF = $('#targetDF').jqxCheckBox('checked');
    var checkedCT = $('#targetCT').jqxCheckBox('checked');
    var checkedMS = event.args.checked;
    if (checkedDF | checkedCT | checkedMS) {
      $('#targetCheck').css({
        'border-style': 'none',
        'border-width': '0px',
        'border-color': '',
      });
      ok = false;
    } else {
      $('#targetCheck').css({
        'border-style': 'solid',
        'border-width': '1px',
        'border-color': '#ff0000',
      });
      ok = true;
    }
    $('#btnFind').jqxButton({ disabled: ok });
    $('#btnClean').jqxButton({ disabled: ok });
  });

  // Text input: admin email
  if (!$('#txtEmail').jqxInput('val')) {
    ok = false;
    $('#txtEmail').css('border-color', '#ff0000');
  } else {
    $('#txtEmail').css('border-color', '');
  }

  // Text input: Tag name
  if (!$('#txtTag').jqxInput('val')) {
    ok = false;
    $('#txtTag').css('border-color', '#ff0000');
  } else {
    $('#txtTag').css('border-color', '');
  }

  // Text input: Title
  if (!$('#txtTitle').jqxInput('val')) {
    ok = false;
    $('#txtTitle').css('border-color', '#ff0000');
  } else {
    $('#txtTitle').css('border-color', '');
  }

  $('#btnSave').jqxButton({ disabled: !ok });
}

// Button: Guardar
function _sendSave() {
  console.log($('#txtTimestamp').jqxInput('val'));

  var options = {
    email: $('#txtEmail').jqxInput('val'),
    tagname: $('#txtTag').jqxInput('val'),
    timestamp: $('#txtTimestamp').jqxInput('val'),
    title: $('#txtTitle').jqxInput('val'),
    authors: $('#txtAuthors ').jqxInput('val'),
    doi: $('#txtDoi').jqxInput('val'),
    url: $('#txtUrl').jqxInput('val'),
    publication: $('#dtpPublica').jqxDateTimeInput('getDate'),
    publisher: $('#txtPublisher').jqxInput('val'),
    abstract: $('#txtAbstract').jqxInput('val'),
    pseudocode: $('#txtPsudocode').jqxInput('val'),
    targetDF: $('#targetDF').jqxCheckBox('val'),
    targetCT: $('#targetCT').jqxCheckBox('val'),
    targetMS: $('#targetMS').jqxCheckBox('val'),
    power: Number($('#sldPower').jqxSlider('val')),
    cost: Number($('#sldCost').jqxSlider('val')),
    security: Number($('#sldSecurity').jqxSlider('val')),
    topology: Number($('#sldTopology').jqxSlider('val')),
    distribution: Number($('#sldDistribution').jqxSlider('val')),
    accuracy: Number($('#sldAccuracy').jqxSlider('val')),
    stability: Number($('#sldStability').jqxSlider('val')),
    software: Number($('#sldSoftware').jqxSlider('val')),
    hardware: Number($('#sldHardware').jqxSlider('val')),
  };
  _log('[_sendSave] options = ' + JSON.stringify(options, null, 2));
  $.post('db/save', JSON.stringify(options), (data, status, xhr) => {
    console.log({
      data: data,
      status: status,
      xhr: xhr,
    });
    if (data.err) {
      _notify('Request SAVE FAILED: ' + data.err, 'error');
    } else {
      _notify('Request SAVE OK ', 'success');
    }
  });
}

// Button: Buscar
function _sendFind() {
  _log('searching...');
  $('#btnFind').jqxButton({ disabled: true });
  var options = {
    targetDF: $('#targetDF').jqxCheckBox('val'),
    targetCT: $('#targetCT').jqxCheckBox('val'),
    targetMS: $('#targetMS').jqxCheckBox('val'),
  };
  _log('[_sendFind] options = ' + JSON.stringify(options, null, 2));
  $.post('db/find', JSON.stringify(options), (data, status, xhr) => {
    console.log({
      data: data,
      status: status,
      xhr: xhr,
    });
    if (data.err) {
      _notify('Request FIND FAILED: ' + data.err, 'error');
    } else {
      _notify('Request FIND OK', 'success');
    }
    _searchList(data);
    _softClean();
  });
}

// Button: Clean
function _softClean() {
  _log('[softClean] cleaning...');
  _init();
}

function _sendClean() {
  location.reload();
}

function _searchList(list) {
  _log('[_searchList] input list: ');
  console.log({
    list: list,
  });

  doc = list.res;

  var source = {
    localData: doc,
    dataType: 'json',
    id: 'id',
    dataFields: [
      { name: 'id', type: 'uuid' },
      { name: 'email', type: 'string' },
      { name: 'tagname', type: 'string' },
      { name: 'timestamp', type: 'string' },
      { name: 'title', type: 'string' },
      { name: 'authors', type: 'string' },
      { name: 'doi', type: 'string' },
      { name: 'url', type: 'string' },
      { name: 'publication', type: 'date' },
      { name: 'publisher', type: 'string' },
      { name: 'abstract', type: 'string' },
      { name: 'psudocode', type: 'string' },
      { name: 'targetdf', type: 'bool' },
      { name: 'targetct', type: 'bool' },
      { name: 'targetms', type: 'bool' },
      { name: 'power', type: 'number' },
      { name: 'cost', type: 'number' },
      { name: 'security', type: 'number' },
      { name: 'topology', type: 'number' },
      { name: 'distribution', type: 'number' },
      { name: 'accuracy', type: 'number' },
      { name: 'stability', type: 'number' },
      { name: 'software', type: 'number' },
      { name: 'hardware', type: 'number' },
    ],
    hierarchy: {
      keyDataField: { name: 'id' },
    },
  };
  var dataAdapter = new $.jqx.dataAdapter(source);

  var cellsrenderer = function (
    row,
    columnfield,
    value,
    defaulthtml,
    columnproperties
  ) {
    if (value == 0) {
      return '<div class="jqx-grid-cell-left-align" style="margin-top: 8.5px; color:#cccccc;">N/D</div>';
    } else {
      return (
        '<div class="jqx-grid-cell-left-align" style="margin-top: 8.5px;">' +
        value +
        '</div>'
      );
    }
  };

  // create Tree Grid
  $('#tablelist').jqxGrid({
    width: '100%',
    source: dataAdapter,
    sortable: true,
    columns: [
      { text: 'Tag Name', dataField: 'tagname', minWidth: 100, width: 100 },
      { text: 'Title', dataField: 'title', minWidth: 100, width: 100 },
      {
        text: 'Data Fusion',
        columngroup: 'TargetType',
        columntype: 'checkbox',
        dataField: 'targetdf',
        minWidth: 20,
        width: 70,
      },
      {
        text: 'Cooperative Transmissions',
        columngroup: 'TargetType',
        columntype: 'checkbox',
        dataField: 'targetct',
        minWidth: 20,
        width: 70,
      },
      {
        text: 'MAC Schemes',
        columngroup: 'TargetType',
        columntype: 'checkbox',
        dataField: 'targetms',
        minWidth: 20,
        width: 70,
      },
      {
        text: 'Power',
        dataField: 'power',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Cost',
        dataField: 'cost',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Security',
        dataField: 'security',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Topology',
        dataField: 'topology',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Distribution',
        dataField: 'distribution',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Accuracy',
        dataField: 'accuracy',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Stability',
        dataField: 'stability',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Software',
        dataField: 'software',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Hardware',
        dataField: 'hardware',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
    ],
    columngroups: [{ text: 'Targets', align: 'center', name: 'TargetType' }],
  });

  $('#tablelist').on('rowclick', function (event) {
    var args = event.args;
    // row's bound index.
    var boundIndex = args.rowindex;
    // row's visible index.
    var visibleIndex = args.visibleindex;
    // right click.
    var rightclick = args.rightclick;
    // original event.
    var ev = args.originalEvent;

    // row data.
    var row = args.row;
    // row bounddata.
    _bounddata = row.bounddata;

    // original click event.
    var clickEvent = args.originalEvent;
    console.log('Selected row with boundIndex: ' + boundIndex);

    // change button name
    $('#btnSave').jqxButton({ value: 'Edit' });
    $('#btnDelete').jqxButton({ disabled: false });

    $('#txtEmail').jqxInput('val', _bounddata.email);
    $('#txtTag').jqxInput('val', _bounddata.tagname);
    $('#txtTimestamp').jqxInput('val', _bounddata.timestamp);
    $('#txtTitle').jqxInput('val', _bounddata.title);
    $('#txtAuthors').jqxInput('val', _bounddata.authors);
    $('#txtDoi').jqxInput('val', _bounddata.doi);
    $('#txtUrl').jqxInput('val', _bounddata.url);
    $('#dtpPublica').jqxDateTimeInput({
      value: new Date(_bounddata.dtapublication),
    });
    $('#txtPublisher').jqxInput('val', _bounddata.publisher);
    $('#txtAbstract').jqxInput('val', _bounddata.abstract);
    $('#targetDF').jqxCheckBox('val', _bounddata.targetdf);
    $('#targetCT').jqxCheckBox('val', _bounddata.targetct);
    $('#targetMS').jqxCheckBox('val', _bounddata.targetms);
    $('#sldPower').jqxSlider('val', _bounddata.power);
    $('#sldCost').jqxSlider('val', _bounddata.cost);
    $('#sldSecurity').jqxSlider('val', _bounddata.security);
    $('#sldTopology').jqxSlider('val', _bounddata.topology);
    $('#sldDistribution').jqxSlider('val', _bounddata.distribution);
    $('#sldAccuracy').jqxSlider('val', _bounddata.accuracy);
    $('#sldStability').jqxSlider('val', _bounddata.stability);
    $('#sldSoftware').jqxSlider('val', _bounddata.software);
    $('#sldHardware').jqxSlider('val', _bounddata.hardware);

    _loadChart(_bounddata);
  });
}

function _sendDelete() {
  console.log('Deleting ' + _bounddata.id);

  var options = {
    id: _bounddata.id,
  };
  _log('[_sendDelete] options = ' + JSON.stringify(options, null, 2));
  $.post('db/delete', JSON.stringify(options), (data, status, xhr) => {
    console.log({
      data: data,
      status: status,
      xhr: xhr,
    });
    if (data.err) {
      _notify('Request DELETE FAILED: ' + data.err, 'error');
    } else {
      _notify('Request DELETE OK', 'success');
    }
  });
}

function _loadChart(data) {
  _log('[_loadChart] data: ');
  console.log({
    data: data,
  });

  if (!data.tagname || !data.tagname.length) {
    _notify('There is no data with the selected values', 'warning');
    $('#radarchart').hide();
  } else {
    $('#radarchart').show();
  }

  Highcharts.chart('radarchart', {
    chart: {
      polar: true,
    },
    title: {
      text: 'Time Synchronization parameters',
      x: -80,
    },
    pane: {
      size: '80%',
    },
    xAxis: {
      categories: [
        'Power consumption',
        'Monetary cost',
        'Security',
        'Network Topology',
        'Message Distribution',
        'Accuracy',
        'Clock Stability',
        'Software Abstraction',
        'Hardware Abstraction',
      ],
      tickmarkPlacement: 'on',
      lineWidth: 0,
    },
    yAxis: {
      gridLineInterpolation: 'polygon',
      lineWidth: 0,
      min: 0,
      max: 10,
      tickInterval: 1,
    },
    tooltip: {
      shared: true,
      headerFormat:
        '<span style="font-size:14px; font-weight: bold">{point.key}</span><table>',
      pointFormat:
        '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true,
    },
    legend: {
      align: 'right',
      verticalAlign: 'middle',
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0,
      },
    },
    series: [
      {
        name: data.tagname,
        type: 'area',
        // type: 'line',
        data: [
          data.power,
          data.cost,
          data.security,
          data.topology,
          data.distribution,
          data.accuracy,
          data.stability,
          data.software,
          data.hardware,
        ],
        pointPlacement: 'on',
      },
    ],
  });
  $('#radarchart').find('.highcharts-credits').remove();
}

function _log(text) {
  if (_enableLog)
    console.log(new Date().toISOString().slice(-13, -1) + ' | ' + text);
}
