/* 
MIT License

Copyright (c) 2020 Francisco Tirado-Andrés "frta@b105.upm.es"

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

var _enableLog = true;

$(document).ready(function () {
  try {
    // Main layout
    $.jqx.theme = 'bootstrap';
    $('#notifier').jqxNotification({
      width: 'auto',
      position: 'bottom-right',
      opacity: 1,
      autoOpen: false,
      animationOpenDelay: 600,
      autoClose: true,
      autoCloseDelay: 5000,
    });
    _init();
  } catch (ex) {
    alert(ex.stack, 'error');
  }
});

function _notify(msg, template) {
  template = typeof template !== 'undefined' ? template : 'info';
  $('#notifier').jqxNotification({ template: template, autoClose: true });
  $('#notifier span').text(msg);
  $('#notifier').jqxNotification('open');
}

function _init(data) {
  _initControls();
}

function _initControls() {
  // Button: Admin
  $('#btnAdmin').jqxButton({ disabled: false });
  $('#btnAdmin').on('click', function () {
    if ($('#btnAdmin').jqxButton('disabled')) return;
    _log('btnAdmin clicked!');
    window.location = 'admin.html';
  });

  // Button: User
  $('#btnUser').jqxButton({ disabled: false });
  $('#btnUser').on('click', function () {
    if ($('#btnUser').jqxButton('disabled')) return;
    _log('btnUser clicked!');
    window.location = 'user.html';
  });

  // Button: Calculators
  $('#btnCalc').jqxButton({ disabled: false });
  $('#btnCalc').on('click', function () {
    if ($('#btnCalc').jqxButton('disabled')) return;
    _log('btnCalc clicked!');
    window.location = 'calc.html';
  });
}

function _log(text) {
  if (_enableLog)
    console.log(new Date().toISOString().slice(-13, -1) + ' | ' + text);
}
