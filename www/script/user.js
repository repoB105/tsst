/* 
MIT License

Copyright (c) 2020 Francisco Tirado-Andrés "frta@b105.upm.es"

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

var _enableLog = true;

$(document).ready(function () {
  try {
    // Main layout
    $.jqx.theme = 'bootstrap';
    $('#notifier').jqxNotification({
      width: 'auto',
      position: 'bottom-right',
      opacity: 1,
      autoOpen: false,
      animationOpenDelay: 600,
      autoClose: true,
      autoCloseDelay: 5000,
    });
    _init();
  } catch (ex) {
    alert(ex.stack, 'error');
  }
});

function _notify(msg, template) {
  template = typeof template !== 'undefined' ? template : 'info';
  $('#notifier').jqxNotification({ template: template, autoClose: true });
  $('#notifier span').text(msg);
  $('#notifier').jqxNotification('open');
}

function _init(data) {
  _initControls();
  _validateOptions();
}

function _initControls() {
  /* info popover */
  $('[data-toggle="popover"]').popover({
    html: true,
    container: 'body',
    content: function () {
      var content = $(this).attr('data-popover-content');
      return $(content).children('.popover-body').html();
    },
    title: function () {
      var title = $(this).attr('data-popover-content');
      return $(title).children('.popover-heading').html();
    },
  });

  /* Only one popover at a time */
  $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
      //the 'is' for buttons that trigger popups
      //the 'has' for icons within a button that triggers a popup
      if (
        !$(this).is(e.target) &&
        $(this).has(e.target).length === 0 &&
        $('.popover').has(e.target).length === 0
      ) {
        $(this).popover('hide');
      }
    });
  });

  //Dropdown type: Target
  $('#targetDF').jqxRadioButton({ checked: false });
  $('#targetDF').on('change', _validateOptions);
  $('#targetCT').jqxRadioButton({ checked: false });
  $('#targetCT').on('change', _validateOptions);
  $('#targetMS').jqxRadioButton({ checked: false });
  $('#targetMS').on('change', _validateOptions);
  $('#targetCheck').css({
    'border-style': 'solid',
    'border-width': '1px',
    'border-color': '#ff0000',
  });

  // Power
  $('#sldPower').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 1,
    max: 10,
    ticksFrequency: 1,
    value: 10,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 1) return value;
      if (value == 10) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value >= 1 && value < 2) {
        var myValue = (value - 1) * 45 + 5;
        return '<' + myValue + 'µW';
      } else if (value >= 2 && value < 3) {
        var myValue = (value - 2) * 450 + 50;
        return '<' + myValue + 'µW';
      } else if (value >= 3 && value < 4) {
        var myValue = (value - 3) * 500 + 500;
        return '<' + myValue + 'µW';
      } else if (value >= 4 && value < 5) {
        var myValue = (value - 4) * 4 + 1;
        return '<' + myValue + 'mW';
      } else if (value >= 5 && value < 6) {
        var myValue = (value - 5) * 45 + 5;
        return '<' + myValue + 'mW';
      } else if (value >= 6 && value < 7) {
        var myValue = (value - 6) * 450 + 50;
        return '<' + myValue + 'mW';
      } else if (value >= 7 && value < 8) {
        var myValue = (value - 7) * 500 + 500;
        return '<' + myValue + 'mW';
      } else if (value >= 8 && value <= 9) {
        var myValue = (value - 8) * 4 + 1;
        return '<' + myValue + 'W';
      } else if (value == 10) return 'Indifferent';
      else return value;
    },
  });
  // Power: Validation
  $('#sldPower').on('change', function (event) {
    var value = event.args.value;
    if (value > 9) {
      $('#sldPower').jqxSlider('val', 10);
    }
  });

  // Cost
  $('#sldCost').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 1,
    max: 10,
    ticksFrequency: 1,
    value: 10,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 1) return value;
      if (value == 10) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value >= 1 && value < 2) {
        var myValue = (value - 1) * 14 + 1;
        return '<' + myValue + '%';
      } else if (value >= 2 && value < 3) {
        var myValue = (value - 2) * 15 + 15;
        return '<' + myValue + '%';
      } else if (value >= 3 && value < 4) {
        var myValue = (value - 3) * 10 + 30;
        return '<' + myValue + '%';
      } else if (value >= 4 && value < 5) {
        var myValue = (value - 4) * 10 + 40;
        return '<' + myValue + '%';
      } else if (value >= 5 && value < 6) {
        var myValue = (value - 5) * 10 + 50;
        return '<' + myValue + '%';
      } else if (value >= 6 && value < 7) {
        var myValue = (value - 6) * 15 + 60;
        return '<' + myValue + '%';
      } else if (value >= 7 && value < 8) {
        var myValue = (value - 7) * 15 + 75;
        return '<' + myValue + '%';
      } else if (value >= 8 && value <= 9) {
        var myValue = (value - 8) * 10 + 90;
        return '<' + myValue + '%';
      } else if (value == 10) return 'Indifferent';
      else return value;
    },
  });
  // Validation
  $('#sldCost').on('change', function (event) {
    var value = event.args.value;
    if (value > 9) {
      $('#sldCost').jqxSlider('val', 10);
    }
  });

  // Security
  $('#sldSecurity').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 1,
    max: 10,
    ticksFrequency: 1,
    value: 10,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 1) return value;
      if (value == 10) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 10) return 'Indifferent';
      else return value;
    },
  });
  // Validation
  $('#sldSecurity').on('change', function (event) {
    var value = event.args.value;
    if (value > 9) {
      $('#sldSecurity').jqxSlider('val', 10);
    }
  });

  // Topology
  $('#sldTopology').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 1,
    max: 10,
    ticksFrequency: 1,
    value: 10,
    step: 2,
    tickLabelFormatFunction: function (value) {
      if (value == 1) return value;
      if (value == 10) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 1) return 'Mesh';
      else if (value == 3) return 'Tree';
      else if (value == 5) return 'Star';
      else if (value == 7) return 'Linear';
      else if (value == 9) return 'All valid';
      else if (value == 10) return 'Indifferent';
      else return value;
    },
  });
  // Validation
  $('#sldTopology').on('change', function (event) {
    var value = event.args.value;
    if (value != 10 && !(value % 2)) {
      value += 1;
      $('#sldTopology').jqxSlider('val', value);
    }
  });

  // Distribution
  $('#sldDistribution').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 1,
    max: 10,
    ticksFrequency: 1,
    value: 10,
    step: 2,
    tickLabelFormatFunction: function (value) {
      if (value == 1) return value;
      if (value == 10) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 1) return 'Multi-Hop Broadcast';
      else if (value == 3) return 'Multi-Hop Point-to-Point';
      else if (value == 5) return 'Multi-Hop Flooding';
      else if (value == 7) return 'One-Hop Broadcast';
      else if (value == 9) return 'One-Hop Point-to-Point';
      else if (value == 10) return 'Indifferent';
    },
  });
  // Validation
  $('#sldDistribution').on('change', function (event) {
    var value = event.args.value;
    if (value != 10 && !(value % 2)) {
      value += 1;
      $('#sldDistribution').jqxSlider('val', value);
    }
  });

  // Accuracy
  $('#sldAccuracy').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 1,
    max: 10,
    ticksFrequency: 1,
    value: 10,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 1) return value;
      if (value == 10) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value >= 1 && value < 2) {
        var myValue = (value - 1) * 9 + 1;
        return '<' + myValue + 'µs';
      } else if (value >= 2 && value < 3) {
        var myValue = (value - 2) * 490 + 10;
        return '<' + myValue + 'µs';
      } else if (value >= 3 && value < 4) {
        var myValue = (value - 3) * 500 + 500;
        return '<' + myValue + 'µs';
      } else if (value >= 4 && value < 5) {
        var myValue = (value - 4) * 9 + 1;
        return '<' + myValue + 'ms';
      } else if (value >= 5 && value < 6) {
        var myValue = (value - 5) * 490 + 10;
        return '<' + myValue + 'ms';
      } else if (value >= 6 && value < 7) {
        var myValue = (value - 6) * 500 + 500;
        return '<' + myValue + 'ms';
      } else if (value >= 7 && value < 8) {
        var myValue = (value - 7) * 9 + 1;
        return '<' + myValue + 's';
      } else if (value >= 8 && value <= 9) {
        var myValue = (value - 8) * 50 + 10;
        return '<' + myValue + 's';
      } else if (value == 10) return 'Indifferent';
      else return value;
    },
  });
  // Validation
  $('#sldAccuracy').on('change', function (event) {
    var value = event.args.value;
    if (value > 9) {
      $('#sldAccuracy').jqxSlider('val', 10);
    }
  });

  // Stability
  $('#sldStability').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 1,
    max: 10,
    ticksFrequency: 1,
    value: 10,
    step: 0.25,
    tickLabelFormatFunction: function (value) {
      if (value == 1) return value;
      if (value == 10) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value >= 1 && value < 2) {
        var myValue = (value - 1) * 0.09 + 0.01;
        return '<' + myValue + 'ppb';
      } else if (value >= 2 && value < 3) {
        var myValue = (value - 2) * 0.9 + 0.1;
        return '<' + myValue + 'ppb';
      } else if (value >= 3 && value < 4) {
        var myValue = (value - 3) * 9 + 1;
        return '<' + myValue + 'ppb';
      } else if (value >= 4 && value < 5) {
        var myValue = (value - 4) * 0.09 + 0.01;
        return '<' + myValue + 'ppm';
      } else if (value >= 5 && value < 6) {
        var myValue = (value - 5) * 0.9 + 0.1;
        return '<' + myValue + 'ppm';
      } else if (value >= 6 && value < 7) {
        var myValue = (value - 6) * 9 + 1;
        return '<' + myValue + 'ppm';
      } else if (value >= 7 && value < 8) {
        var myValue = (value - 7) * 90 + 10;
        return '<' + myValue + 'ppm';
      } else if (value >= 8 && value <= 9) {
        var myValue = (value - 8) * 900 + 100;
        return '<' + myValue + 'ppm';
      } else if (value == 10) return 'Indifferent';
      else return value;
    },
  });
  // Validation
  $('#sldStability').on('change', function (event) {
    var value = event.args.value;
    if (value > 9) {
      $('#sldStability').jqxSlider('val', 10);
    }
  });

  // Software
  $('#sldSoftware').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 1,
    max: 10,
    ticksFrequency: 1,
    value: 10,
    step: 1,
    tickLabelFormatFunction: function (value) {
      if (value == 1) return value;
      if (value == 10) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 1) return 'APP OS';
      else if (value == 2) return 'APP BM';
      else if (value == 3) return 'OS';
      else if (value == 4) return 'NET';
      else if (value == 5) return 'MAC';
      else if (value == 6) return 'HAL';
      else if (value == 7) return 'Driver';
      else if (value == 8) return 'Register';
      else if (value == 10) return 'Indifferent';
      else return value;
    },
  });
  // Validation
  $('#sldSoftware').on('change', function (event) {
    var value = event.args.value;
    if (value > 8) {
      $('#sldSoftware').jqxSlider('val', 10);
    }
  });

  // Hardware
  $('#sldHardware').jqxSlider({
    showTickLabels: true,
    tooltip: true,
    mode: 'fixed',
    height: 60,
    min: 1,
    max: 10,
    ticksFrequency: 1,
    value: 10,
    step: 1,
    tickLabelFormatFunction: function (value) {
      if (value == 1) return value;
      if (value == 10) return value;
      return '';
    },
    tooltipFormatFunction: function (value) {
      if (value == 1) return 'HW-NS Low Res';
      else if (value == 2) return 'HW-NS High Res';
      else if (value == 3) return 'Additonal OSC';
      else if (value == 4) return 'External Module';
      else if (value == 5) return 'TX/RX specific';
      else if (value == 6) return 'MCU specific';
      else if (value == 7) return 'Platform specific';
      else if (value == 10) return 'Indifferent';
      else return value;
    },
  });
  // Validation
  $('#sldHardware').on('change', function (event) {
    var value = event.args.value;
    if (value > 7) {
      $('#sldHardware').jqxSlider('val', 10);
    }
  });

  // Button: Buscar
  $('#btnFind').jqxButton({ disabled: true });
  $('#btnFind').on('click', function () {
    if ($('#btnFind').jqxButton('disabled')) return;
    _sendFind();
  });

  // Button: Limpiar
  $('#btnClean').jqxButton({ disabled: false });
  $('#btnClean').on('click', function () {
    if ($('#btnClean').jqxButton('disabled')) return;
    _sendClean();
  });

  // Button: Back
  $('#btnBack').jqxButton({ disabled: false });
  $('#btnBack').on('click', function () {
    if ($('#btnBack').jqxButton('disabled')) return;
    window.location = 'index.html';
  });
}

function _validateOptions() {
  var ok = true;

  // TARGET
  $('#targetDF').bind('change', function (event) {
    var checkedDF = event.args.checked;
    if (checkedDF) {
      $('#targetCheck').css({
        'border-style': 'none',
        'border-width': '0px',
        'border-color': '',
      });
      ok = false;
    } else {
      $('#targetCheck').css({
        'border-style': 'solid',
        'border-width': '1px',
        'border-color': '#ff0000',
      });
      ok = true;
    }
    $('#btnFind').jqxButton({ disabled: ok });
  });
  $('#targetCT').bind('change', function (event) {
    var checkedCT = event.args.checked;
    if (checkedCT) {
      $('#targetCheck').css({
        'border-style': 'none',
        'border-width': '0px',
        'border-color': '',
      });
      ok = false;
    } else {
      $('#targetCheck').css({
        'border-style': 'solid',
        'border-width': '1px',
        'border-color': '#ff0000',
      });
      ok = true;
    }
    $('#btnFind').jqxButton({ disabled: ok });
  });
  $('#targetMS').bind('change', function (event) {
    var checkedMS = event.args.checked;
    if (checkedMS) {
      $('#targetCheck').css({
        'border-style': 'none',
        'border-width': '0px',
        'border-color': '',
      });
      ok = false;
    } else {
      $('#targetCheck').css({
        'border-style': 'solid',
        'border-width': '1px',
        'border-color': '#ff0000',
      });
      ok = true;
    }
    $('#btnFind').jqxButton({ disabled: ok });
  });
}

// Button: Buscar
function _sendFind() {
  var targetname;
  if ($('#targetDF').jqxRadioButton('val')) {
    targetname = 'targetdf';
  } else if ($('#targetCT').jqxRadioButton('val')) {
    targetname = 'targetct';
  } else {
    targetname = 'targetms';
  }
  _log('[_sendFind] target: ' + targetname);

  var options = {
    targetname: $(targetname).selector,
    power: $('#sldPower').val(),
    cost: $('#sldCost').val(),
    security: $('#sldSecurity').val(),
    topology: $('#sldTopology').val(),
    distribution: $('#sldDistribution').val(),
    accuracy: $('#sldAccuracy').val(),
    stability: $('#sldStability').val(),
    software: $('#sldSoftware').val(),
    hardware: $('#sldHardware').val(),
  };
  _log('[_sendFind] options = ' + JSON.stringify(options, null, 2));
  $.post('./db/filter', JSON.stringify(options), (data, status, xhr) => {
    console.log({
      data: data,
      status: status,
      xhr: xhr,
    });

    $('#tablelist').jqxGrid('clear');
    $('#radarchart').hide();
    if (data.res.length == 0) {
      _notify('There is no data with the selected values', 'warning');
    } else {
      _notify('Request FIND OK', 'success');
      _searchList(options, data);
    }
  });
}

function _sendClean() {
  location.reload();
}

function _searchList(filter, list) {
  _log('[_searchList] input list: ');
  console.log({
    filter: filter,
    list: list,
  });

  doc = list.res;

  var source = {
    localData: doc,
    dataType: 'json',
    id: 'timestamp',
    sortcolumn: 'accuracy',
    dataFields: [
      { name: 'id', type: 'uuid' },
      { name: 'email', type: 'string' },
      { name: 'tagname', type: 'string' },
      { name: 'timestamp', type: 'string' },
      { name: 'title', type: 'string' },
      { name: 'authors', type: 'string' },
      { name: 'doi', type: 'string' },
      { name: 'url', type: 'string' },
      { name: 'publication', type: 'date' },
      { name: 'publisher', type: 'string' },
      { name: 'abstract', type: 'string' },
      { name: 'psudocode', type: 'string' },
      { name: 'targetdf', type: 'bool' },
      { name: 'targetct', type: 'bool' },
      { name: 'targetms', type: 'bool' },
      { name: 'power', type: 'number' },
      { name: 'cost', type: 'number' },
      { name: 'security', type: 'number' },
      { name: 'topology', type: 'number' },
      { name: 'distribution', type: 'number' },
      { name: 'accuracy', type: 'number' },
      { name: 'stability', type: 'number' },
      { name: 'software', type: 'number' },
      { name: 'hardware', type: 'number' },
    ],
    hierarchy: {
      keyDataField: { name: 'timestamp' },
    },
  };
  var dataAdapter = new $.jqx.dataAdapter(source);

  _log('[_searchList] dataadapter: ');
  console.log({
    dataAdapter: dataAdapter,
  });

  var cellsrenderer = function (
    row,
    columnfield,
    value,
    defaulthtml,
    columnproperties
  ) {
    if (value == 0) {
      return '<div class="jqx-grid-cell-left-align" style="margin-top: 8.5px; color:#cccccc;">N/D</div>';
    } else {
      return (
        '<div class="jqx-grid-cell-left-align" style="margin-top: 8.5px;">' +
        value +
        '</div>'
      );
    }
  };

  var initrowdetails = function (
    index,
    parentElement,
    gridElement,
    datarecord
  ) {
    var rowDetails = $($(parentElement).children()[0]);
    rowDetails.html('<div><b>Title:</b> ' + datarecord.title + '</div></br>');
    if (datarecord.authors) {
      rowDetails.append(
        '<div><b>Authors:</b> ' + datarecord.authors + '</div></br>'
      );
    }
    if (datarecord.doi) {
      rowDetails.append(
        "<div><b>DOI:</b><a href='https://doi.org/" +
          datarecord.doi +
          "' target='_blank'> " +
          datarecord.doi +
          '</a></div></br>'
      );
    }
    if (datarecord.url) {
      rowDetails.append(
        "<div><b>URL:</b><a href='" +
          datarecord.url +
          "' target='_blank'> " +
          datarecord.url +
          '</div></br>'
      );
    }
    if (datarecord.publication) {
      rowDetails.append(
        '<div><b>Publication date:</b> ' +
          datarecord.publication +
          '</div></br>'
      );
    }
    if (datarecord.publisher) {
      rowDetails.append(
        '<div><b>Journal:</b> ' + datarecord.publisher + '</div></br>'
      );
    }
    if (datarecord.abstract) {
      rowDetails.append(
        '<div><b>Abstract:</b> ' + datarecord.abstract + '</div></br>'
      );
    }
    if (datarecord.pseudocode) {
      rowDetails.append(
        '<div><b>Pseudocode:</b> ' + datarecord.pseudocode + '</div></br>'
      );
    }
  };

  // create Grid
  $('#tablelist').jqxGrid({
    width: '100%',
    source: dataAdapter,
    sortable: true,
    columnsresize: true,
    rowdetails: true,
    rowdetailstemplate: {
      rowdetails:
        "<div style='margin: 10px; width: 1000px; height: 150px; overflow: auto; white-space: pre-wrap;'>Row Details</div>",
      rowdetailsheight: 160,
      rowdetailshidden: true,
    },
    ready: function () {
      $('#tablelist').jqxGrid('hiderowdetails', 0);
    },
    initrowdetails: initrowdetails,
    columns: [
      { text: 'Tag Name', dataField: 'tagname', minWidth: 50, width: 100 },
      { text: 'Title', dataField: 'title', minWidth: 100, width: 100 },
      {
        text: 'Data Fusion',
        columngroup: 'TargetType',
        columntype: 'checkbox',
        dataField: 'targetdf',
        minWidth: 30,
        width: 70,
      },
      {
        text: 'Cooperative Transmissions',
        columngroup: 'TargetType',
        columntype: 'checkbox',
        dataField: 'targetct',
        minWidth: 30,
        width: 70,
      },
      {
        text: 'MAC Schemes',
        columngroup: 'TargetType',
        columntype: 'checkbox',
        dataField: 'targetms',
        minWidth: 30,
        width: 70,
      },
      {
        text: 'Power',
        dataField: 'power',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Cost',
        dataField: 'cost',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Security',
        dataField: 'security',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Topology',
        dataField: 'topology',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Distribution',
        dataField: 'distribution',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Accuracy',
        dataField: 'accuracy',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Stability',
        dataField: 'stability',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Software',
        dataField: 'software',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
      {
        text: 'Hardware',
        dataField: 'hardware',
        minWidth: 20,
        width: 70,
        cellsrenderer: cellsrenderer,
      },
    ],
    columngroups: [{ text: 'Targets', align: 'center', name: 'TargetType' }],
  });

  var returnData = list.res;
  var rowExpanded = -1;
  returnData.unshift(filter); // insert as first element
  _loadChart(returnData);

  $('#tablelist').on('rowclick', function (event) {
    var args = event.args;
    // row's bound index.
    var boundIndex = args.rowindex;
    // row's visible index.
    var visibleIndex = args.visibleindex;
    // right click.
    var rightclick = args.rightclick;
    // original event.
    var ev = args.originalEvent;
    // row data.
    var row = args.row;
    // row bounddata.
    var bounddata = row.bounddata;

    _log('[rowclick] args: ');
    console.log({
      args: args,
    });

    console.log('Selected row with boundIndex: ' + boundIndex);
    var rowSize = $('#tablelist').jqxGrid('getrows').length;
    for (var i = 0; i < rowSize + 1; i++) {
      $('#tablelist').jqxGrid('hiderowdetails', i);
    }

    _rowSelect = boundIndex;

    if (rowExpanded == boundIndex) {
      $('#tablelist').jqxGrid('hiderowdetails', boundIndex);
      rowExpanded = -1;
    } else {
      $('#tablelist').jqxGrid('showrowdetails', boundIndex);
      rowExpanded = boundIndex;
      $('#tablelist').jqxGrid('selectrow', boundIndex);
    }
  });
}

function _loadChart(data) {
  _log('[_loadChart] data: ');
  console.log({
    data: data,
  });

  if (data.length <= 1) {
    _notify('There is no data with the selected values', 'warning');
    $('#radarchart').hide();
  } else {
    $('#radarchart').show();
  }

  plotData = data.map(function (s, i) {
    if (i == 0) {
      return {
        name: 'User Filter',
        type: 'area',
        fillOpacity: 0.35,
        pointPlacement: 'on',
        data: [
          Number(s.power),
          Number(s.cost),
          Number(s.security),
          Number(s.topology),
          Number(s.distribution),
          Number(s.accuracy),
          Number(s.stability),
          Number(s.software),
          Number(s.hardware),
        ],
      };
    } else {
      return {
        name: s.tagname,
        type: 'line',
        pointPlacement: 'on',
        data: [
          Number(s.power),
          Number(s.cost),
          Number(s.security),
          Number(s.topology),
          Number(s.distribution),
          Number(s.accuracy),
          Number(s.stability),
          Number(s.software),
          Number(s.hardware),
        ],
      };
    }
  });
  _log('[_loadChart] plotdata: ');
  console.log({
    plotData: plotData,
  });

  Highcharts.chart('radarchart', {
    chart: {
      polar: true,
    },
    title: {
      text: 'Graphical representation of the Time-Synchronization Strategies',
      x: 0,
    },
    pane: {
      size: '100%',
    },
    xAxis: {
      categories: [
        'Power consumption',
        'Monetary cost',
        'Security',
        'Network Topology',
        'Message Distribution',
        'Accuracy',
        'Clock Stability',
        'Software Abstraction',
        'Hardware Abstraction',
      ],
      tickmarkPlacement: 'on',
      lineWidth: 0,
    },
    yAxis: {
      gridLineInterpolation: 'polygon',
      lineWidth: 0,
      min: 0,
      max: 10,
      tickInterval: 1,
    },
    legend: {
      align: 'center',
      verticalAlign: 'bottom',
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0,
      },
    },
    series: plotData,
  });
  $('#radarchart').find('.highcharts-credits').remove();
}

function _log(text) {
  if (_enableLog)
    console.log(new Date().toISOString().slice(-13, -1) + ' | ' + text);
}
